package eav

import (
	"bytes"
	"context"
	"reflect"
	"text/template"
)

type (
	// EAV is this packages default Interface
	EAVErr interface {
		error
		AddCause(cause ...error)
		Cause(causer error) error
		Causes() []error
		Code() int
		Context() context.Context
		Equal(equal error) bool
		HasCause(causer error) bool
		IgnoreCauses(ignore bool)
		Is(is error) bool
		String() string
		Template() string
		TemplateData() map[string]interface{}
	}

	eavError struct {
		causeSep     string
		causes       []error
		code         int
		context      context.Context
		ignoreCauses bool
		msg          string
		origin       error
		template     string
		templateData map[string]interface{}
	}

	eavErrorOption func(*eavError)
)

var (
	// Ensure eavError implements the EAVErr interface.
	_ EAVErr = &eavError{}

	// templateWriter (re)used to render error templates.
	templateWriter = bytes.NewBuffer(nil)
)

// new is used internally to return a new eavError pointer
func (eavError) new(msg string) *eavError {
	return &eavError{msg: msg, causeSep: " "}
}

// parseTemplate is used internally to parse any template bound to this error instance.
// It takes care of binding error messages created from Go template parsing.
func (e *eavError) parseTemplateError(msg string) string {
	// Only parse if we have a template string bound
	if e.template == "" {
		return msg
	}

	// On template parse error, we add the parse error to this error message
	// and return as we cannot Execute a non parsable template.
	t, err := template.New("").Parse(e.template)
	if err != nil {
		if msg != "" {
			msg = msg + e.causeSep
		}
		return msg + err.Error()
	}

	// We have successfully parsed a template, reset the buffer for execution.
	templateWriter.Reset()

	// As t.Execute can return an error, we add the error message as required.
	err = t.Execute(templateWriter, e.TemplateData())
	if err != nil {
		if msg != "" {
			msg = msg + e.causeSep
		}
		msg = msg + err.Error()
	}
	if msg != "" {
		msg = msg + e.causeSep
	}
	return msg + templateWriter.String()
}

// NewError will create a new EAVErr interface instance.
func NewError(msg string, opts ...eavErrorOption) EAVErr {
	e := eavError{}.new(msg)
	for _, opt := range opts {
		opt(e)
	}
	return e
}

// NewFromError will create a new EAVErr interface instance from an existing error.
func NewFromError(err error, opts ...eavErrorOption) EAVErr {
	var msg string
	if err != nil {
		msg = err.Error()
	}
	e := eavError{}.new(msg)
	e.origin = err
	for _, opt := range opts {
		opt(e)
	}
	return e
}

// IsEAVErr will return true if the error implements the EAVErr interface
func IsEAVErr(err error) bool {
	return ToEAVErr(err) != nil
}

// ToEAVErr will EAVErr interface if able, otherwise nil. Use IsEAVErr to check if ok.
func ToEAVErr(err error) EAVErr {
	if eav, ok := err.(EAVErr); ok {
		return eav
	}
	return nil
}

// AddCause to the current error instance. Note that we iterate to check that the
// cause being added is not the same instance as the parent for safety.
func (e *eavError) AddCause(cause ...error) {
	for _, c := range cause {
		if e != c {
			e.causes = append(e.causes, c)
		}
	}
}

// Cause will attempt to return an error by causer type. Will return the first of type if found.
func (e *eavError) Cause(causer error) error {
	if causer == nil || len(e.causes) == 0 {
		return nil
	}

	t := reflect.TypeOf(causer)
	for _, c := range e.causes {
		if reflect.TypeOf(c) == t {
			return c
		}
	}
	return nil
}

// Causes will return a slice of all error causes.
func (e *eavError) Causes() []error {
	return e.causes
}

// Code will return the int value associated with this error. Useful for act upon when processing.
func (e *eavError) Code() int {
	return e.code
}

// Context will return nil or the context bound to this error instance.
func (e *eavError) Context() context.Context {
	return e.context
}

// Error implements the go std lib error interface. It will render and concatenate bounded causes and templates.
func (e *eavError) Error() string {
	// Parse any templates bound to this error first, using the msg as the source/base to concatenate from.
	msg := e.parseTemplateError(e.msg)

	// Ignore causes in the error message
	if !e.ignoreCauses {
		for _, c := range e.Causes() {
			m := c.Error()
			if m != "" {
				var sep string
				if msg != "" {
					sep = e.causeSep
				}
				msg = msg + sep + m
			}
		}
	}

	return msg
}

// Equal checks if the origin is equal to the equal error type
func Equal(origin, equal error) bool {
	eaverr := ToEAVErr(origin)
	if eaverr != nil {
		return eaverr.Equal(equal)
	}
	return origin == equal
}

// Equal will return boolean on whether the origin of this instance is equal
func (e *eavError) Equal(equal error) bool {
	if e == equal {
		return true
	}
	return e.origin == equal
}

// HasCause checks if the error passes is of type EAVErr and will check if it has the error cause.
func HasCause(origin, cause error) bool {
	eaverr := ToEAVErr(origin)
	if eaverr != nil {
		return eaverr.HasCause(cause)
	}
	return false
}

// HasCause will return boolean on whether the causer is one of the causes bound.
func (e *eavError) HasCause(causer error) bool {
	return e.Cause(causer) != nil
}

// IgnoreCauses will set the flag that determines whether causes bound to this error
// instance should be ignored in the returned error message.
func (e *eavError) IgnoreCauses(ignore bool) {
	e.ignoreCauses = ignore
}

// Is checks if the origin "is" equal OR the origin Has an "is"
func Is(origin, is error) bool {
	if Equal(origin, is) {
		return true
	}

	return HasCause(origin, is)
}

// Is will return true if the underlying origin of this EAVError is of "is" error type.
func (e *eavError) Is(is error) bool {
	return e.origin == is
}

// String implements the Stringer interface.
func (e *eavError) String() string {
	return e.Error()
}

// Template will return a string to parse an error string using go templates.
func (e *eavError) Template() string {
	return e.template
}

// TemplateData will return data to render a parsed error template string.
func (e *eavError) TemplateData() map[string]interface{} {
	if e.templateData == nil {
		return make(map[string]interface{})
	}
	return e.templateData
}

// With Funcs

// WithCause will bind an error cause to a EAV error instance.
func WithCause(err error) eavErrorOption {
	return func(e *eavError) {
		e.causes = append(e.causes, err)
	}
}

// WithCauseSeparator separates each cause with this string value, Default is a single space.
func WithCauseSeparator(sep string) eavErrorOption {
	return func(e *eavError) {
		e.causeSep = sep
	}
}

// WithCode will bind a code to a EAV error instance.
func WithCode(code int) eavErrorOption {
	return func(e *eavError) {
		e.code = code
	}
}

// WithContext will bind a context to a EAV error instance. Use this to bind
// any "type" to this Error instance for post processing.
func WithContext(ctx context.Context) eavErrorOption {
	return func(e *eavError) {
		e.context = ctx
	}
}

// WithIgnoreCauses will ignore each of the error causes error messages
func WithIgnoreCauses(ignore bool) eavErrorOption {
	return func(e *eavError) {
		e.ignoreCauses = ignore
	}
}

// WithTemplate will bind a string template to parse against this EAV error instance.
func WithTemplate(template string) eavErrorOption {
	return func(e *eavError) {
		e.template = template
	}
}

// WithTemplateData is called to bind data for a template.
func WithTemplateData(data map[string]interface{}) eavErrorOption {
	if data == nil {
		data = make(map[string]interface{})
	}
	return func(e *eavError) {
		e.templateData = data
	}
}
