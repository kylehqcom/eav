# EAV - Errors Are Values

A Go package to enrich the handling of errors in your applications.

## But why though?

Errors are values, yes. These values however, in my opinion, are dependent entirely on the audience that is digesting these values.
If consuming system to system errors that will only be viewed internally by the maintainers of "said" systems, then the existing [https://github.com/pkg/errors](https://github.com/pkg/errors) package is likely a better fit for you.

However, if your application errors are consumed not only by an engineering audience, but also bubble up to a public user interface, then this package may be of use to you. This is a far more likely for non _"enterprise"_ based projects.

## Ok, how can this package help me?

### 1 - Causes are supplementary

This means that the origin error is the default error message to be rendered.
In contrast to the [https://github.com/pkg/errors](https://github.com/pkg/errors) package which attempts to return the origin based on whether the nested error implements the `causer` interface. Refer [https://github.com/pkg/errors/blob/master/errors.go#L275](https://github.com/pkg/errors/blob/master/errors.go#L275)

```go
// If the error does not implement Cause, the original error will
// be returned. If the error is nil, nil will be returned without further
// investigation.
func Cause(err error) error {
	type causer interface {
		Cause() error
    }
    ...
```

The errorPkg iterates over all `causers` and when it detects that the error does **not** implement the interface, it **assumes** that the current iteration error is the origin error.
That to me is flawed as the origin error returned is now bound/relies on whether the developer was even aware of implementing the interface. So the origin error, by design, has a much higher probability of being incorrect.
Instead, I have opted to make the origin the default, and causes are secondary/supplementary info to provide context.

### 2 - Error fields

An EAVErr contains additional fields to give better context to your errors.

- A `Code` field of type `int` allows for defining a unique or group of errors types for post processing
- The `Context` field allows the caller to add _\*any_ application specific values to the error
- Potentially more in future to follow for more context, eg a layer/component field for example

### 3 - Templates

If you would prefer, you can pass a valid Go template string and template data to render an error message instead.
Allowing for more flexibility in error message generation.

### 4 - Fine grained error message control

- The `EAVErr` implements the `Stringer` interface so all printing will render the Error() string.
- You can bind an explicit separator between each of the error clauses.
  Or you can ignore clause rendering altogether.
- Easily check if your error _Has_ a specific clause to alter calling logic.

## Enough already, show me an example

```go

import "gitlab.com/kylehqcom/eav"

// customError implements the error interface and is used in your app
type customError struct {
	error
}

func (e customError) Error() string {
	return "A custom error string."
}

err := eav.NewError("Bad happened!", eav.WithCause(fmt.Errorf("Due to a cause.")), eav.WithCause(customError{}))
err.Error() // "Bad happened! Due to a cause. A custom error string."
err.IgnoreCauses(true)
err.Error() // "Bad happened!"

// Has checks on causes of the origin error
err.HasCause(customError{}) // true

// IsEAVErr checks if err is of this package type EAVErr
eav.IsEAVErr(err) // true

// Is checks that err is either equal or has a customError{} cause
eav.Is(err, customError{}) // true

// Equal checks that err == customError{}
eav.Equal(err, customError{}) // false

tpl := `Number: {{.number}} Label: {{.label}}`
data := map[string]interface{}{"number": 24, "label": "hooray"}
errWithTemplate := eav.NewError("", eav.WithTemplate(tpl), eav.WithTemplateData(data))
errWithTemplate.Error() // "Number: 24 Label: hooray"

errWithTemplate = eav.NewError("With new msg:", eav.WithTemplate(tpl), eav.WithTemplateData(data))
errWithTemplate.Error() // "With new msg: Number: 24 Label: hooray"

// Create from an existing error too
sepErr := eav.NewFromError(customError{},
	eav.WithCause(fmt.Errorf("Due to a cause.")),
	eav.WithCauseSeparator(" ||| "),
)
sepErr.AddCause(fmt.Errorf("Due to a cause2."), fmt.Errorf("Due to a cause3."))
sepErr.Error() // "A custom error string. ||| Due to a cause. ||| Due to a cause2. ||| Due to a cause3."

sepErr.HasCause(customError{}) // false as customError{} is the origin
sepErr.Is(customError{}) // true
sepErr.Equal(customError{}) // true as sepErr == customError{}
```
