package eav_test

import (
	"context"
	"fmt"
	"strconv"
	"testing"

	"gitlab.com/kylehqcom/eav"
)

type testError struct {
	error
}

func (te testError) Error() string {
	return "test error"
}

func logFail(t *testing.T, have, expects string) {
	t.Log("Have:", have)
	t.Log("Expects:", expects)
	t.Fail()
}

func TestCauses(t *testing.T) {
	t.Parallel()
	e := eav.NewError("err")
	if len(e.Causes()) != 0 {
		logFail(t, strconv.Itoa(len(e.Causes())), "0")
	}

	// Adding an error as a cause to itself will not error, but won't add either
	e.AddCause(e, e, e)
	if 0 != len(e.Causes()) {
		t.Log("Cannot add self as cause")
		t.Fail()
	}

	if e.HasCause(nil) {
		t.Log("Nil cause must be false")
		t.Fail()
	}

	// Add a test error cause
	e.AddCause(testError{})
	if 1 != len(e.Causes()) {
		t.Log("Should have one testError cause")
		t.Fail()
	}

	if !e.HasCause(testError{}) {
		t.Log("Has testError should be true")
		t.Fail()
	}

	goErr := fmt.Errorf("go err")
	if e.HasCause(goErr) {
		t.Log("Has no go error, should be false")
		t.Fail()
	}

	e.AddCause(goErr)
	if !e.HasCause(goErr) {
		t.Log("Has go error, should be true")
		t.Fail()
	}
}

func TestEqual(t *testing.T) {
	t.Parallel()
	te := testError{}
	if !eav.Equal(te, te) {
		t.Log("testError is a testError")
		t.Fail()
	}

	e := eav.NewFromError(te)
	if !eav.Equal(e, te) {
		t.Log("NewFromError has origin of testError")
		t.Fail()
	}

	if !eav.Equal(e, e) {
		t.Log("EAV Error is equal to EAV Error")
		t.Fail()
	}

	if eav.Equal(te, e) {
		t.Log("testError is not equal to EAV Error")
		t.Fail()
	}

	// Add the cause to ensure assertions between is & equals
	e.AddCause(te)

	if !eav.Equal(e, te) {
		t.Log("error has testError so is should be true")
		t.Fail()
	}
	if eav.Equal(te, e) {
		t.Log("testError is not equal to EAV Error")
		t.Fail()
	}
}

func TestHasCause(t *testing.T) {
	t.Parallel()
	te := testError{}
	if eav.HasCause(te, te) {
		t.Log("testError is not an EAV Error")
		t.Fail()
	}

	err := eav.NewFromError(te)
	if eav.HasCause(err, te) {
		t.Log("EAV Error has no causes")
		t.Fail()
	}

	err.AddCause(te)
	if !eav.HasCause(err, te) {
		t.Log("EAV Error has a testError cause")
		t.Fail()
	}
}

func TestIs(t *testing.T) {
	t.Parallel()
	te := testError{}
	if !eav.Is(te, te) {
		t.Log("testError is a testError")
		t.Fail()
	}

	e := eav.NewFromError(te)
	if !eav.Is(e, te) {
		t.Log("NewFromError has origin of testError")
		t.Fail()
	}

	if !eav.Is(e, e) {
		t.Log("EAV Error is equal to EAV Error")
		t.Fail()
	}

	if eav.Is(te, e) {
		t.Log("testError is not equal to EAV Error")
		t.Fail()
	}

	// HasCause should return true for Is
	e.AddCause(te)
	if !eav.Is(e, te) {
		t.Log("error has testError so is should be true")
		t.Fail()
	}
}

func TestIsEAVError(t *testing.T) {
	t.Parallel()
	te := testError{}
	if eav.IsEAVErr(te) {
		t.Log("testError is not an EAV Error")
		t.Fail()
	}

	e := eav.NewFromError(te)
	if !eav.IsEAVErr(e) {
		t.Log("NewFromError is a valid EAV Error")
		t.Fail()
	}
}

func ToEAVErr(t *testing.T) {
	t.Parallel()
	te := testError{}
	if nil != eav.ToEAVErr(te) {
		t.Log("testError is not a valid EAV Error")
		t.Fail()
	}

	stdErrReturn := func() error {
		return eav.NewError("returns the std error interface")
	}

	eavErr := eav.ToEAVErr(stdErrReturn())
	if eavErr == nil {
		t.Log("should had a valid EAV Error")
		t.Fail()
	}
	eav.HasCause(nil, nil)
}

func TestNewError(t *testing.T) {
	t.Parallel()
	expects := "Bad happened!"
	e := eav.NewError(expects)
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}
}

func TestNewErrorFromError(t *testing.T) {
	t.Parallel()
	expects := "Bad happened!"
	e := eav.NewFromError(fmt.Errorf(expects))
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}
}

func TestNewErrorIgnoreCauses(t *testing.T) {
	t.Parallel()
	e := eav.NewError("Bad happened!", eav.WithCause(fmt.Errorf("Due to a cause.")), eav.WithIgnoreCauses(true))
	expects := "Bad happened!"
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	e.IgnoreCauses(false)
	expects = "Bad happened! Due to a cause."
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	e.AddCause(fmt.Errorf("Due to a cause2."), fmt.Errorf("Due to a cause3."))
	expects = "Bad happened! Due to a cause. Due to a cause2. Due to a cause3."
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	e.IgnoreCauses(true)
	expects = "Bad happened!"
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}
}

func TestNewErrorWithCause(t *testing.T) {
	t.Parallel()
	e := eav.NewError("Bad happened!", eav.WithCause(fmt.Errorf("Due to a cause.")))
	expects := "Bad happened! Due to a cause."
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	e.AddCause(fmt.Errorf("Due to a cause2."), fmt.Errorf("Due to a cause3."))
	expects = "Bad happened! Due to a cause. Due to a cause2. Due to a cause3."
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}
}

func TestWithCauseSeparator(t *testing.T) {
	t.Parallel()
	e := eav.NewError("Bad happened!",
		eav.WithCause(fmt.Errorf("Due to a cause.")),
		eav.WithCauseSeparator(" ||| "),
	)
	e.AddCause(fmt.Errorf("Due to a cause2."), fmt.Errorf("Due to a cause3."))
	expects := "Bad happened! ||| Due to a cause. ||| Due to a cause2. ||| Due to a cause3."
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}
}

func TestWithContext(t *testing.T) {
	t.Parallel()
	ctx := context.WithValue(context.Background(), "foo", "bar")
	e := eav.NewError("with ctx", eav.WithContext(ctx))
	if e.Context() != ctx {
		t.Log("Invalid context")
		t.Fail()
	}
}

func TestWithTemplate(t *testing.T) {
	t.Parallel()
	tpl := `Number: {{.number}} Label: {{.label}}`
	data := map[string]interface{}{"number": 24, "label": "hooray"}
	causeWithTemplate := eav.NewError("", eav.WithTemplate(tpl), eav.WithTemplateData(data))

	e := eav.NewFromError(causeWithTemplate)
	expects := "Number: 24 Label: hooray"
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	// Add more causes to confirm nested template execute
	expects = "Number: 24 Label: hooray Number: 24 Label: hooray"
	e.AddCause(causeWithTemplate)

	// We loop so that we ensure that the reusable template writer clears it's buffer on each write.
	for i := 0; i < 3; i++ {
		if e.Error() != expects {
			logFail(t, e.Error(), expects)
		}
	}

	causeWithTemplate = eav.NewError("With new msg:", eav.WithTemplate(tpl), eav.WithTemplateData(data))
	e = eav.NewFromError(causeWithTemplate)
	expects = "With new msg: Number: 24 Label: hooray"
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	// Add more causes to confirm nested template execute
	expects = "With new msg: Number: 24 Label: hooray With new msg: Number: 24 Label: hooray"
	e.AddCause(causeWithTemplate)

	// We loop so that we ensure that the reusable template writer clears it's buffer on each write.
	for i := 0; i < 3; i++ {
		if e.Error() != expects {
			logFail(t, e.Error(), expects)
		}
	}

	// Check we render correctly when we have bad data bound to the template
	var bad func()
	data = map[string]interface{}{"number": bad}
	causeWithTemplateBadData := eav.NewError("", eav.WithTemplate(tpl), eav.WithTemplateData(data))
	e = eav.NewFromError(causeWithTemplateBadData)
	expects = `template: :1:10: executing "" at <{{.number}}>: can't print {{.number}} of type func() Number: `
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	// Add a valid error cause to ensure the error still renders related msgs too
	e.AddCause(causeWithTemplate)
	expects = `template: :1:10: executing "" at <{{.number}}>: can't print {{.number}} of type func() Number:  With new msg: Number: 24 Label: hooray`
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	// Now lets make a bad template to confirm that the template.Parse error is bound to
	// the error message returned
	tpl = `{{kaboom}}`
	causeWithBadTemplate := eav.NewError("", eav.WithTemplate(tpl))
	e = eav.NewFromError(causeWithBadTemplate)
	expects = `template: :1: function "kaboom" not defined`
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}

	// Add a valid error cause to ensure the error still renders related msgs too
	e.AddCause(causeWithTemplate)
	expects = `template: :1: function "kaboom" not defined With new msg: Number: 24 Label: hooray`
	if e.Error() != expects {
		logFail(t, e.Error(), expects)
	}
}
